<?php 
require __DIR__.'/../boot/boot.php';


use Hotel\Room;
use Hotel\Favorite;
use Hotel\User;
use Hotel\Review;
use Hotel\Booking;



//initialize Room service
$room = new Room;
//$roomType = new RoomType;
$favorite = new Favorite;

//Check for room id
$roomId = $_REQUEST['room_id'];
if(empty($roomId)){
    header('Location: landing_page.php');
    return;
}

//Load room info
$roomInfo = $room->get($roomId);

if(empty($roomInfo)) {
    header('Location: landing_page.php');
    return;
}
// //load room type info
// $roomTypeInfo = $roomType->get($roomId);

//Get current user id
$userId = User::getCurrentUserId();

//Check if room is favorite for current user
$isFavorite = $favorite->isFavorite($roomId, $userId);

//Load all room reviews
$review = new Review();
$allReviews = $review->getReviewsByRoom($roomId);

//Check for booking room
$checkInDate = $_REQUEST['check_in_date'];
$checkOutDate = $_REQUEST['check_out_date'];



$alreadyBooked = empty($checkInDate) || empty($checkOutDate);
if(!$alreadyBooked){
    //Look for bookings
    $booking = new Booking();
    $alreadyBooked = $booking->isBooked($roomId,$checkInDate, $checkOutDate);

}



?>



<!DOCTYPE>
<html>
    <head>
        <meta name="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,nofollow">
        <title>Room info</title>
        <style type="text/css">
            body {
                background: #fff;
            }
        </style>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script src="../assets/pages/room.js"></script>
    </head>
    <body>
        <header class="header">
            <div class="container">
                <p class="main-logo">Hotels</p>
                <div class="primary-menu text-right">
                    <ul>
                        <li>
                            <a href="landing_page.php" target="_blank">
                            <i class="fas fa-home"></i>
                        Home</a>
                    </li>
                    <li>
                        <a class="prof" href="profile_page.php" target="_blank">
                            <i class="fas fa-user"></i>
                            
                            Profile
                        </a>
                    </li>
                    <li>
                        <a class="door">
                            <i class="fas fa-door-open"></i> 
                        </a>
                    </li>
                    </ul>
            </div>
        </header>
        <main class="main-content page-home">
            <div class="container">
                <section class="hotel-info">
                    <header class="hotel-title">
                        <h4 class="hotel-name inline"><?php echo sprintf('%s - %s, %s', $roomInfo['name'], $roomInfo['city'], $roomInfo['area']); ?> | <div class="title-reviews inline" > Reviews: 

                            <?php 
                            $roomAvgReview = $roomInfo['avg_reviews'];
                            for ($i = 1; $i <= 5; $i++) {
                                if ($roomAvgReview >= $i) {
                                    ?>
                                    <i class="fa fa-star checked"></i>
                                    <?php 
                                } else {
                                    ?>
                                    <i class="fas fa-star"></i>
                                    <?php
                                     } 
                                }
                            ?>
                             | 
                             </div>
                            <div class="favorite inline" id="favorite">
                                <form name="favoriteForm" method="post" id="favoriteForm" class="favoriteForm" action="actions/favorite.php">
                                    <input type="hidden" name="room_id" value="<?php echo $roomId; ?>">
                                    <input type="hidden" name="is_favorite" value="<?php echo $isFavorite ? '1' : '0'; ?>">
                                    <input type="hidden" name="csrf" value="<?php echo User::getCsrf(); ?>">
                                    
                                            <div  class="heart submit inline <?php echo $isFavorite ? 'selected' : '';  ?>" id="fav"><i class="fas fa-heart"></i> 
                                            </div> 
                                   
                                </form>

                                
                            </div>



                        
                    </h4><div class="per inline">Per Night: <?php echo $roomInfo['price'] ?></div>
                    </header>  
                    
                    <div class="hotel-img">
                        <img src="../assets/images/rooms/room-1.jpg" alt="Welcome to our site" width="100%" height="auto">
                    </div>
                    <section class="extra-info">
                        <div class="guests">
                           <div class="text-center"> <i class="fas fa-user"></i> <?php echo $roomInfo['count_of_guests']?></div>
                            COUNT OF GUESTS
                        </div>
                        <div class="typeofroom">
                            <div class="text-center"><i class="fas fa-bed"></i> <?php echo $roomInfo['type_id'] ?></div>
                            TYPE OF ROOM
                        </div>
                        <div class="parking">
                            <div class = "text-center">
                                <i class="fas fa-parking"></i>
                                <?php echo $roomInfo['parking']?>
                            </div> 
                            PARKING
                        </div>
                        <div class="wifi">
                            <div>
                                <i class="fas fa-wifi"></i>
                                <?php if($roomInfo['wifi'] == 1) {
                                    echo "Yes";
                                }else echo "No"; ?>
                            </div> 
                            WIFI
                        </div>
                        <div class="pet">
                            <div class = "text-center">
                                <?php if($roomInfo['pet_friendly'] == 1) {
                                    echo "Yes";
                                }else echo "No"; ?>
                            </div>
                            PET FRIENDLY
                        </div>
                    </section>
                    <main class="info">
                    <h4>Room Description</h4>
                    <p><?php echo $roomInfo['description_long']  ?></p>
                </main>
            </section>

            <?php
                if(!empty(User::getCurrentUserId())){

            ?>
            <div class=" text-right">
                <?php
                if($alreadyBooked){
                ?>
                <span class= "booked">Already Booked</span>
                    <?php
                     }else{
                    ?>
                <form name="bookingForm" method="post" action="actions/booking.php">
                    <input type="hidden" name="room_id" value="<?php echo $roomId ?>">
                    <input type="hidden" name="check_in_date" value="<?php echo $checkInDate?>">
                    <input type="hidden" name="check_out_date" value="<?php echo $checkOutDate?>">
                    <input type="hidden" name="csrf" value="<?php echo User::getCsrf(); ?>">

                    <button type="submit">Book Now</button>    
                </form>
                <?php
                    }
                    ?>
            </div>

            <?php
                }
                ?>



            <section class="reviews">
                <div class="existing-reviews">
                    <h1>Reviews</h1>
                    <div class="room-reviews-container">
                        <?php 
                            foreach ($allReviews as $counter => $review ) {    
                        ?>
                            <div class="room-reviews">
                            
                                
                                    <h4><?php echo sprintf('%d. %s', $counter+1, $review['user_name']); ?>
                                    <?php 
                                // $roomAvgReview = $roomInfo['avg_reviews'];
                                for ($i = 1; $i <= 5; $i++) {
                                    if ($review['rate'] >= $i) {
                                        ?>
                                        <i class="fa fa-star checked"></i>
                                        <?php 
                                    } else {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <?php
                                         } 
                                    }
                                ?>
                                    </h4> 
                                    <h5>add time: <?php echo $review['created_time']; ?> </h5>
                                    <div class="review-text"> <?php echo htmlentities($review['comment']); ?></div>
                            </div>
                    <?php
                        }
                ?>
                </div>
            </div>
                <div class="add-review">
                    <h1>Add Review</h1>
                    <form name="reviewForm" method="post" id="stars-form" class="stars-form" action="actions/review.php">
                        <input type="hidden" name="room_id" value="<?php echo $roomId ?>">
                        <input type="hidden" name="csrf" value="<?php echo User::getCsrf(); ?>">

    <div class="star-rating">
    	 <input type="radio" id="star1" name="rate" value="1"/><i></i>
    	 <input type="radio" id="star2" name="rate" value="2"/><i></i>
    	 <input type="radio" id="star3" name="rate" value="3"/><i></i>
    	 <input type="radio" id="star4" name="rate" value="4"/><i></i>
         <input type="radio" id="star5" name="rate" value="5"/><i></i>
        
    </div>
                    <textarea id="reviewField" class="text-review" name="comment" form="stars-form" placeholder="Review" rows="4" cols="50"></textarea>
                    <div class="action text-center">
                            <input name="submit" id="submitButton" type="submit" value="Submit">
                        </div>
                </form>
                </div>
            </section>

</div>
</main>
<footer>
            <p>(c) Copyright CollegeLink 2021</p>
        </footer>
        
        <link rel="stylesheet" href="../assets/css/fontawesome.min.css" />
        <link rel="stylesheet" type="text/css" href="room_styles.css">
        <link rel="stylesheet" type="text/css" href="globalStyles.css">
        <script src="./fav_sub.js"></script>
        <script src="./logout.js"></script>


        </body>
</html>