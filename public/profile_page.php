<?php

require __DIR__.'/../boot/boot.php';

use Hotel\Favorite;
use Hotel\Review;
use Hotel\Booking;
use Hotel\User;


//Check for logged in user
$userId = User::getCurrentUserId();
if(empty($userId)){
    header('Location: landing_page.php');
    return;
}

//Get all favorites
$favorite = new Favorite();
$userFavorites = $favorite->getListByUser($userId);

//Get all reviews
$review = new Review();
$userReviews = $review->getListByUser($userId);

//Get all user bookings
$booking = new Booking();
$userBookings = $booking->getListByUser($userId);

?>


<!DOCTYPE>
<html>
    <head>
        <meta name="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,nofollow">
        <title>Profile page</title>
        <style type="text/css">
            body {
                background: #fff;
            }
        </style>
    </head>
    <body>
        <header class="header">
            <div class="container">
                <p class="main-logo">Hotels</p>
                <!-- <div class="main-logo">Hotels</div> -->
                <div class="primary-menu text-right">
                    <ul>
                        <li>
                            <a href="landing_page.php" target="_blank">
                            <i class="fas fa-home"></i>
                        Home</a>
                    </li>
                    <li>
                       
                        <a class="prof" href="profile.php" target="_blank">
                            
                         <i class="fas fa-user"></i> Profile 
                        </a>
                    </li>
                    <li>
                        <a class="door">
                            <i class="fas fa-door-open"></i> 
                        </a>
                    </li>
                    </ul>
            </div>
        </header>
        <main class="main-content page-home">
            <div class="container">
                <aside class="side-box box">
                    <div class="favorites">
                        <header class="sidebar-title text-left">
                            <h4>FAVORITES</h4>
                        </header>
                        <?php 
                        if(count($userFavorites) > 0){
                            ?>
                        <ol>
                            <?php
                            foreach ($userFavorites as $favorite) {
                             ?>
                            <li style="font-size: 22px"><a href="room_page.php?room_id=<?php echo $favorite['room_id'];?>"><span style="font-size:16px"><?php echo $favorite['name']; ?></span></a></li>
                            <?php
                        }
                        ?>
                        </ol>
                        <?php
                    }else{
                    ?>
                    <h4>You don't have any favorite Hotels yet</h4>
                    <?php
                }
                ?>

                    </div>
                    <div class="reviews">
                        <header class="sidebar-title text-left">
                            <h4>REVIEWS</h4>
                        </header>
                        <?php 
                        if(count($userReviews) > 0){
                            ?>
                        <ol>
                            <?php
                            foreach ($userReviews as $review) {
                             ?>
                            <li style="font-size: 22px"><a href="room_page.php?room_id=<?php echo $review['room_id'];?>"><span style="font-size:16px"><?php echo $review['name'] ?></span></a></li>
                            <?php

                            $roomAvgReview = $roomInfo['avg_reviews'];
                            for ($i = 1; $i <= 5; $i++) {
                                if ($review['rate'] >= $i) {
                                    ?>
                                    <i class="fa fa-star checked"></i>
                                    <?php 
                                } else {
                                    ?>
                                    <i class="fas fa-star"></i>
                                    <?php
                                     } 
                                }
                        
                             }
                             ?>
                        </ol>
                        <?php 
                            }else{
                            ?>
                            <h4>You haven't made any reviews yet</h4>
                            <?php
                        }
                        ?>
                    </div>
                </aside>
               
                
                <section class="booking-list box">
                    <header class="page-title">
                        <h2>My bookings</h2>
                    </header>
                     <?php
                if(count($userBookings) > 0){
                    ?>
                    <div class="list-style">
                        <?php
                        foreach ($userBookings as $booking) {
                        ?>
                    <article class="hotel">
                        <aside class="media">
                            <img src="../assets/images/rooms/<?php echo $booking['photo_url'];?>" alt="Welcome to our site" width="100%" height="auto">
                        </aside>
                        <main class="info">
                            <div class="hotel-title"><?php echo $booking['name'];?></div>
                            <div class="location"><?php echo sprintf('%s, %s', $booking['city'], $booking['area']);?></div>
                            <p><?php echo $booking['description_short'];?></p>
                            <div class="text-right">
                                <button><a href="room_page.php?room_id=<?php echo $booking['room_id'];?>" target="_blank">Go to room page</a></button>
                            </div>
                            
                    
                        </main>
                    <div class=total-price>
                            Total Cost:<?php echo $booking['total_price'];?>
                        </div>
                    <section class="extra-info">
                        <div class="inDate">
                            Check-in Date: <?php echo $booking['check_in_date'];?> 
                        </div>
                        <div class="between1">|</div>
                        <div class="outDate">
                            Check-out Date: <?php echo $booking['check_out_date'];?>
                        </div>
                        <div class="between1">|</div>
                        
                        <div class="room-type">
                            Type of Room: <?php echo $booking['room_type'];?>
                        </div>
                    </section>
                        
                    </article>
                    <?php
                }
                ?>
                </div>

             
                    <!-- <article class="hotel">
                        <aside class="media">
                            <img src="../assets/images/rooms/room-2.jpg" alt="Welcome to our site" width="100%" height="auto">
                        </aside>
                        <main class="info">
                        <div class="hotel-title">Hotel Title</div>
                            <div class="location">Location</div>
                            <p>This is my awesome site This is my awesome site This is my awesome site This is my awesome site This is my awesome site This is my awesome site .</p>
                        
                        <div class="text-right">
                            <button><a href="room_page.html" target="_blank">Go to room page</a></button>
                        </div>

                    </main>
                
                    </article> -->
                    <?php
            }else{
                ?>
            <h4> You don't have any bookings yet</h4>
            <?php
        }
        ?>
                </section>
                
            </div>
    </main>
    <footer>
            <p>(c) Copyright CollegeLink 2021</p>
        </footer>
        
        <link rel="stylesheet" href="../assets/css/fontawesome.min.css" />

        <link rel="stylesheet" type="text/css" href="profile_styles.css">
        <link rel="stylesheet" type="text/css" href="globalStyles.css">
        <script src="./logout.js"></script>
        
    </body>
</html>