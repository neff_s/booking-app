<?php 

use Hotel\Review;
use Hotel\User;

//Boot application

require_once __DIR__. '/../../boot/boot.php';

//Return to home page if not post request
if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
	echo "This is a post script";
	die;
}
//if no user is logged in, return to main page
if(empty(User::getCurrentUserId() ) ){
	echo "No current user for this operation";
    die; 

}

// Check if room id is given
$roomId = $_REQUEST['room_id'];
if (empty($roomId)){
	echo "No room is given for this operation";
	die;
}

//Verify csrf
$csrf = $_REQUEST['csrf'];
if(empty($csrf) || !User::verifyCsrf($csrf)) {
	echo "this is an invalid request";

	return;
}

//Add review
$review = new Review();
$review->insert($roomId, User::getCurrentUserId(), $_REQUEST['rate'], $_REQUEST['comment']);

//Get all reviews
$roomReviews = $review->getReviewsByRoom($roomId);
$counter = count($roomReviews);
//Load user
$user = new User();
$userInfo = $user->getByUserId(User::getCurrentUserId());
?>

<div class="room-reviews">
    <h4><?php echo sprintf('%d. %s', $counter, $userInfo['name']); ?>
    <?php 
	for ($i = 1; $i <= 5; $i++) {
	    if ($_REQUEST['rate'] >= $i) {
	        ?>
	        <i class="fa fa-star checked"></i>
	        <?php 
	} else {
        ?>
        <i class="fa fa-star"></i>
        <?php
         } 
    }
?>
    </h4> 
    <h5>add time: <?php echo (new DateTime())->Format('Y-m-d H:i:s'); ?> </h5>
    <div class="review-text"> <?php echo $_REQUEST['comment']; ?></div>
</div>