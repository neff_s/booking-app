<?php

require __DIR__.'/../../boot/boot.php';

use Hotel\Room;
//use DateTime;
use Hotel\RoomType;


//initialize Room service
$room = new Room;

//Get distinct Cities
$cities = $room->getCities();


//Get all areas
$areas = $room->getAreas();

//Get all room types
$type = new RoomType();
$allTypes = $type->getAllTypes(); 

//Get page parameters
$selectedCity = $_REQUEST['city'];

$selectedTypeId = $_REQUEST['roomType'];

$checkInDate = $_REQUEST['checkinDate'];

$checkOutDate = $_REQUEST['checkoutDate'];
$countOfGuests = $_REQUEST['count_of_guests'];

if(!empty($_GET['amount-min'])){
  $minimunPrice=$_GET['amount-min'];
}
else{
  $minimunPrice=0;
}
if(!empty($_GET['amount-max'])){
  $maximumPrice=$_GET['amount-max'];
}
else{
  $maximumPrice=0;
}



//Search for a room
$allAvailableRooms = $room->search(new DateTime($checkInDate), new DateTime($checkOutDate), $selectedCity, $selectedTypeId, $minimunPrice, $maximumPrice, $countOfGuests);



//Get count of guests
$guests = $room->getGuests();

?>

<section class="hotel-list box">
    <header class="page-title">
        <h2>Search Results</h2>
    </header>
    <?php
        foreach ($allAvailableRooms as $availableRoom) {
         ?>
    <article class="hotel">
        <aside class="media">
            <img src="../assets/images/rooms/<?php echo $availableRoom['photo_url']; ?>" alt="Welcome to our site" width="100%" height="auto">
        </aside>
        <main class="info">
            <div class="hotel-title">
             <?php 
             echo $availableRoom['name']; ?> 

            </div>
            <div class="location"><?php echo sprintf('%s, %s',$availableRoom['city'], $availableRoom['area']); ?></div>
            <p><?php echo $availableRoom['description_short']; ?></p>
            <div class="text-right">
              <form name="gotoRoomForm"  action="room_page.php">
                    <input type="hidden" name="check_in_date" value="<?php echo $checkInDate?>">
                    <input type="hidden" name="check_out_date" value="<?php echo $checkOutDate?>">
                    <input type="hidden" name="room_id" value="<?php echo $availableRoom['room_id'] ?>">

                    <button type="submit">Go to room page</button>    
                </form>
            </div>

        </main>
        <div class=price>
            Per night: <?php echo $availableRoom['price']; ?>
        </div>
        <div class="extra-info">
                                <div class="guests">
                                    Count of Guests: <?php echo $availableRoom['count_of_guests']; ?>        
                                </div>
                                <div class="between">|</div>
                                <div class="room-type">
                                    Type of Room: <?php echo $availableRoom['title'];?>
                                </div>
                                <div class="clear"></div>
                            </div>
    </article>
     <?php
     
         }
        ?>
    
    <?php if (count($allAvailableRooms) == 0) { ?>

    <h2 class="check-search"> There are no available rooms </h2>
    <?php
     }
      ?>
</section>