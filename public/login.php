<?php

require __DIR__.'/../boot/boot.php';

use Hotel\User;
//Check for existing logged in user, return to main page
if(!empty(User::getCurrentUserId())){

  header('Location:/public/landing_page.php');die;
}

?>



<!DOCTYPE>
<html>
    <head>
        <meta name="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,nofollow">
        <title>Sign in</title>
        <style type="text/css">
            body {
                background: #fff;
            }
        </style>
        <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    />
    </head>
    <body>
        <header class="header">
            <div class="container">
                <p class="main-logo">Hotels</p>
                <div class="primary-menu text-right">
                    <ul>
                        <li>
                            <a href="landing_page.php" target="_blank">
                            <i class="fas fa-home"></i>
                        Home</a>
                    </li>
                    </ul>
            </div>
        </header>

<main class="main-content page-home">
    <section class="hero">
        <form method="POST" action="actions/login.php">
            <h1 class="sign text-center">Sign in</h1>
            <div class="form-group">

              <label for="email">Email address</label>
              <input
                type="input"
                id="email"
                name="email"
              />
              <div class="text-danger email-error">
                Must be a valid email address!
              </div>
            
              <label for="password">Password</label>
              <input
                type="password"
                id="password"
                name="password"
                
              />
              <div class="text-danger password-error">
                Password must be more than 4 characters!
              </div>
              <button type="submit">
              Sign in
            </button>
        </div>
          </form>
      </section>
    </main>
    <link rel="stylesheet" href="../assets/css/fontawesome.min.css" />
    <link rel="stylesheet" type="text/css" href="login_styles.css">
    <link rel="stylesheet" type="text/css" href="globalStyles.css">
    <script src="./login_valid.js"></script>
    </body>
</html>