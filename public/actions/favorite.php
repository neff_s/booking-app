<?php 

use Hotel\Favorite;
use Hotel\User;

//Boot application

require_once __DIR__. '/../../boot/boot.php';

//Return to home page if not post request
if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
	header('Location:/');

	return;
}

//if no user is logged in, return to main page
if(empty(User::getCurrentUserId() ) ){

  header('Location:/');
  return; 

}

// Check if room id is given
$roomId = $_REQUEST['room_id'];
if (empty($roomId)){
	header('Location:/public/landing_page.php');
	return;
}

//Verify csrf
$csrf = $_REQUEST['csrf'];
if(empty($csrf) || !User::verifyCsrf($csrf)) {
	header('Location: /');

	return;
}
//Add or remove room from favorites
$favorite = new Favorite();

$isFavorite = $_REQUEST['is_favorite'];

if (!$isFavorite){

	$status = $favorite->addFavorite($roomId, User::getCurrentUserId());

} else{
	$favorite->removeFavorite($roomId, User::getCurrentUserId());
}


//Return to room page
header(sprintf('Location:/public/room_page.php?room_id=%s', $roomId));

?>