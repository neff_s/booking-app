<?php

//Boot application

require_once __DIR__. '/../../boot/boot.php';

use Hotel\User;

//Return to home page if not post request
if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
	header('Location:/');

	return;
}

$user = new User();



//verify user
try{
	if (!$user->verify($_REQUEST['email'], $_REQUEST['password'])){
		header('Location: /public/login.php?error=Could not verify user');
		return;
	}
}catch(InvalidArgumentException $ex){
	header('Location: /public/login.php?error=No user exists with the given email');
		return;
}




//Retrieve user
$userInfo = $user->getByEmail($_REQUEST['email']);

//Generate token
$token = $user->generateToken($userInfo['user_id']);

//Set cookie
setcookie('user_token',$token,time()+(30*24*60*60),'/');

//Return to home page
header('Location:/public/landing_page.php');


