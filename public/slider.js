jQuery("#slider-range").slider({
  range: true,
  min: 0,
  max: 5000,
  values: [0, 5000],
  slide: function(event, ui) {
    jQuery("#amount-min").val(ui.values[0]);
    jQuery("#amount-max").val(ui.values[1]);
  }
});
jQuery("#amount-min").val(jQuery("#slider-range").slider("values", 0));
jQuery("#amount-max").val(jQuery("#slider-range").slider("values", 1));

