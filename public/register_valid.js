document.addEventListener("DOMContentLoaded", () => {
  const $form = document.querySelector("form");
  const $email = document.querySelector("#email");
  const $emailRepeat = document.querySelector("#email_repeat");
  const $password = document.querySelector("#password");
  const $emailError = document.querySelector(".email-error");
  const $passwordError = document.querySelector(".password-error");
  const $emailRepError = document.querySelector(".repeat-error");
  const $name = document.querySelector("#name");
  const $nameError = document.querySelector(".name-error");

  const getValidations = ({ email, password, email_rep, name}) => {
    let emailIsValid = false;
    let passwordIsValid = false;
    let emailsMatch = false;
    let nameIsValid = false;


    if (
      email !== "" &&
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)
    ) {
      emailIsValid = true;
    }

    if (password !== "" && password.length > 4) {
      passwordIsValid = true;
    }

    if(email === email_rep){
      emailsMatch = true;
      
    } 

    if(name !== ""){
      nameIsValid = true;
    }

    return {
      emailIsValid,
      passwordIsValid,
      emailsMatch,
      nameIsValid
    };
  };

  $form.addEventListener("submit", (e) => {
    e.preventDefault();
    const { email, password, email_rep, name } = e.target.elements;
    const values = {
      email: email.value,
      password: password.value,
      email_rep: email_rep.value,
      name: name.value
    };
    const validations = getValidations(values);

    if (!validations.emailIsValid) {
      $email.classList.add("is-invalid");
      $emailError.classList.remove("d-none");
    } else {
      $email.classList.remove("is-invalid");
      $emailError.classList.add("d-none");
    }

    if (!validations.passwordIsValid) {
      $password.classList.add("is-invalid");
      $passwordError.classList.remove("d-none");
    } else {
      $password.classList.remove("is-invalid");
      $passwordError.classList.add("d-none");
    }

    if (!validations.emailsMatch) {
      $emailRepeat.classList.add("is-invalid");
      $emailRepError.classList.remove("d-none");
    } else {
      $emailRepeat.classList.remove("is-invalid");
      $emailRepError.classList.add("d-none");
    }

    if (!validations.nameIsValid) {
      $name.classList.add("is-invalid");
      $nameError.classList.remove("d-none");
    } else {
      $name.classList.remove("is-invalid");
      $nameError.classList.add("d-none");
    }

    if (validations.emailIsValid && validations.passwordIsValid && validations.emailsMatch && validations.nameIsValid){
      $form.submit();
    }
  });

  $emailError.classList.add("d-none");
  $passwordError.classList.add("d-none");
  $emailRepError.classList.add("d-none");
  $nameError.classList.add("d-none");
});
