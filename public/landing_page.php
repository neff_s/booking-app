<?php

require __DIR__.'/../boot/boot.php';

use Hotel\Room;
use Hotel\RoomType;


//Get Cities
$room = new Room();
$cities = $room->getCities();

//Get all room types
$type = new RoomType();
$allTypes = $type->getAllTypes();
?>


<!DOCTYPE>
<html>
    <head>
        <meta name="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,nofollow">
        <title>Search for a hotel</title>
        <link
            rel="stylesheet"
            href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css"
        />
        <style type="text/css">
            body {
                background: #fff;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="container">
                <p class="main-logo">Hotels</p>
                <div class="primary-menu text-right">
                    <ul>
                        <li>
                            <a href="landing_page.php" target="_blank">
                            <i class="fas fa-home"></i>
                        Home</a>
                    </li>
                    <li>
                        <a class="door">
                            <i class="fas fa-door-open"></i> 
                        </a>
                    </li>
                    </ul>
            </div>
        </header>

<main class="main-content page-home">
    <section class="hero">
        <form action="list_page.php">
            <aside class=form-left>
                <div class="form-group city">
                    
                    <select id="formCity" name="city">
                        <option value="null" selected>City</option>
                        <?php
                            foreach ($cities as $city) {
                         ?>
                            <option value="<?php echo $city; ?>"><?php echo $city; ?></option>
                        <?php
                         }
                        ?>
                    </select>
                </div>
                <div class="form-group checkinDate">
                    <input id="date_in" class="datepicker" placeholder="Check-in Date" name="check_in_date" type="text">
                </div>
            </aside>
            <section class="form-right">
                <div class="form-group roomType">
                    <select id="formRoomType" name="room_type">
                        <option value="null" selected>Room Type</option>
                        <?php
                            foreach ($allTypes as $roomType) {
                         ?>
                            <option value="<?php echo $roomType['type_id']; ?>"><?php echo $roomType['title']; ?></option>
                        <?php
                         }
                        ?>                       

                    </select>
                </div>
            
                <div class="form-group checkoutDate">
                    <input id="date_out" class="datepicker" placeholder="Check-out Date" name="check_out_date" type="text">
                </div>
            </section>
            <div class="action text-center">
                <input name="submit" id="submitButton" type="submit" value="Search">
            </div>
        </form>
    </section>
   <!--  <section class="hero">
        <section class="image-container">
            <img src="assets/images/skg.jpg" alt="Welcome to our site" width="100%" height="auto">
        </section>
    </section> -->
</main>
        <footer>
            <p>(c) Copyright CollegeLink 2021</p>
        </footer>
        
        <link rel="stylesheet" href="../assets/css/fontawesome.min.css" />
        <link rel="stylesheet" type="text/css" href="landpage_styles.css">
        <link rel="stylesheet" type="text/css" href="globalStyles.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="./datepick.js"></script>
        <script src="./logout.js"></script>

    </body>
</html>