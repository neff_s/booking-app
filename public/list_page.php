<?php

require __DIR__.'/../boot/boot.php';

use Hotel\Room;
// use \DateTime;
use Hotel\RoomType;


//initialize Room service
$room = new Room;

//Get distinct Cities
$cities = $room->getCities();

//Get all areas
$areas = $room->getAreas();

//Get all room types
$type = new RoomType();
$allTypes = $type->getAllTypes(); 

//Get page parameters
$selectedCity = $_REQUEST['city'];
$selectedTypeId = $_REQUEST['room_type'];
$checkInDate = $_REQUEST['check_in_date'];
$checkOutDate = $_REQUEST['check_out_date'];
// print_r($selectedTypeId);die;

//Search for a room
$allAvailableRooms = $room->search(new DateTime($checkInDate), new DateTime($checkOutDate), $selectedCity, $selectedTypeId);



//Get count of guests
$guests = $room->getGuests();

?>


<!DOCTYPE>
<html>
    <head>
        <meta name="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,nofollow">
        <title>Search Results</title>
        <style type="text/css">
            body {
                background: #fff;
            }
        </style>
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
      <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css"/>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="../assets/pages/search.js"></script>

    </head>
    <body>
        <header class="header">
            <div class="container">
                <p class="main-logo">Hotels</p>
                <div class="primary-menu text-right">
                    <ul>
                        <li>
                            <a href="landing_page.php" target="_blank">
                            <i class="fas fa-home"></i>
                        Home</a>
                    </li>
                    <li>
                        <a class="prof" href="profile_page.php" target="_blank">
                            <i class="fas fa-user"></i>
                            Profile
                        </a>
                    </li>
                    <li>
                        <a class="door">
                            <i class="fas fa-door-open"></i> 
                        </a>
                    </li>
                    </ul>
            </div>
        </header>
        <main class="main-content page-home">
            <div class="container">
                <aside class="hotel-search box">
                    <header class="sidebar-title text-center">
                        <h2>FIND THE PERFECT <br> HOTEL</h2>
                    </header>
                    <form action="list_page.php" class="searchForm">
                        <div class="form-group CountOfGuests">

                            <select id="formGuests" name="count_of_guests">
                                <option value="null" selected>Count of Guests</option>
                                <?php
                                $index=0;
                                foreach ($guests as $guestCount) {
                             ?>
                                <option value="<?php echo $guestCount[$index]; ?>"><?php echo $guestCount[$index]; ?></option>
                                
                            <?php
                           
                             }
                            ?>
                            </select>
                        </div>
                        <div class="form-group roomType">
                            <select id="formRoomType"  name="roomType">
                                <option value="null">Room Type</option>
                                <?php
                                foreach ($allTypes as $roomType) {
                             ?>
                                <option <?php echo $selectedTypeId == $roomType['type_id'] ? 'selected="selected" ' : ''; ?>value="<?php echo $roomType['type_id']; ?>"><?php echo $roomType['title']; ?></option>
                            <?php
                             }
                            ?>
                            </select>
                        </div>
                        <div class="form-group city">
                            <select id="formCity" name="city">
                                <option value="null" >City</option>
                                <?php
                                foreach ($cities as $city) {
                             ?>
                                <option <?php echo $selectedCity == $city ? 'selected="selected" ' : ''; ?>value="<?php echo $city; ?>"><?php echo $city; ?></option>
                            <?php
                             }
                            ?>
                            </select>
                        </div>
                        <div class="priceSlider">
                            <div class="minAmount">
                                <input type="text" name="amount-min" id="amount-min" readonly>
                            </div>
                            <div class="maxAmount">
                                <input type="text" name="amount-max" id="amount-max" readonly>
                            </div>
                              <div class="slide">
                              <div id="slider-range" class="slide"></div>
                          </div>
                              
                          </div>
                        
                        <div class="form-group checkinDate">
                            <input id="checkinDate" class="datepicker" value="<?php echo $checkInDate; ?>" placeholder="Check-in Date" type="text"  name="checkinDate">
                        </div>
                        <div class="form-group checkoutDate">
                            <input  name="checkoutDate" id="checkoutDate" class="datepicker" value="<?php echo $checkOutDate; ?>" placeholder="Check-out Date" type="text">
                        </div>
                        <div class="action">
                            <input name="submit" id="submitButton" type="submit" value="FIND HOTEL">
                        </div>
                    </form>
                </aside>
                <div id="search-results-container" class="hotel-list">
                    <section class="hotel-list box">
                        <header class="page-title">
                            <h2>Search Results</h2>
                        </header>
                        <?php
                            
                            foreach ($allAvailableRooms as $availableRoom) {
                             ?>
                        <article class="hotel">
                            <aside class="media">
                                <img src="../assets/images/rooms/<?php echo $availableRoom['photo_url']; ?>" alt="Welcome to our site" width="100%" height="auto">
                            </aside>
                            <main class="info">
                                <div class="hotel-title">
                                 <?php 
                                 echo $availableRoom['name']; ?> 

                                </div>
                                <div class="location"><?php echo sprintf('%s, %s',$availableRoom['city'], $availableRoom['area']); ?></div>
                                <p><?php echo $availableRoom['description_short']; ?></p>
                                <div class="text-right">
                                    <form name="gotoRoomForm"  action="room_page.php">
                                        <input type="hidden" name="check_in_date" value="<?php echo $checkInDate?>">
                                        <input type="hidden" name="check_out_date" value="<?php echo $checkOutDate?>">
                                        <input type="hidden" name="room_id" value="<?php echo $availableRoom['room_id'] ?>">

                                        <button type="submit">Go to room page</button>    
                                    </form>
                                </div>

                            </main>
                            <div class=price>
                                Per night: <?php echo $availableRoom['price']; ?>
                            </div>
                            <div class="extra-info">
                                <div class="guests">
                                    Count of Guests: <?php echo $availableRoom['count_of_guests']; ?>        
                                </div>
                                <div class="between">|</div>
                                <div class="room-type">
                                    Type of Room: <?php echo $availableRoom['title'];?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </article>
                         <?php
                         
                             }
                            ?>
                        
                        <?php if (count($allAvailableRooms) == 0) { ?>

                        <h2 class="check-search"> There are no available rooms </h2>
                        <?php
                         }
                          ?>
                    </section>
                </div>
            </div>
    </main>
        <footer>
            <p>(c) Copyright CollegeLink 2021</p>
        </footer>
        
        <link rel="stylesheet" href="../assets/css/fontawesome.min.css" />

        <link rel="stylesheet" type="text/css" href="list_styles.css">
        <link rel="stylesheet" type="text/css" href="globalStyles.css">
        
        <script src="./datepick.js"></script>
        <script src="./logout.js"></script>
         <script src="./slider.js"></script>



    </body>
</html>