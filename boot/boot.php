<?php
error_reporting(E_ERROR);
// Register autoload function
spl_autoload_register(function ($class) {
	$class = str_replace("\\", "/", $class);
	//var_dump(sprintf('app/%s.php', $class));
    require_once sprintf(__DIR__.'/../app/%s.php', $class);
});

use Hotel\User;

$user = new User;

//Check if there is a token request
$userToken = $_COOKIE['user_token'];

if($userToken){
	//Verify user
	if($user->verifyToken($userToken)){
		//Set user in memory
		$userInfo = $user->getTokenPayload($userToken);
		User::setCurrentUserId($userInfo['user_id']);
		//var_dump($userInfo);
	}

}

