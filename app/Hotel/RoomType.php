<?php

namespace Hotel;
use PDO;
use DateTime;
use Hotel\BaseService;

class RoomType extends BaseService
{
	public function getAllTypes()
	{
		return  $this->fetchAll('SELECT * FROM room_type');
	}

	// public function get($roomId)
	// {
	// 	$parameters=[
	// 		':room_id' => $roomId
	// 	];
	// 	return $this->fetch('SELECT title FROM room_type INNER JOIN room on room.type_id=room_type.type_id where room_id = :room_id',$parameters);
	// }
}

