<?php

namespace Hotel;
use PDO;
// use Support\Configuration\Configuration;
use Hotel\BaseService;

class User extends BaseService
{
	const TOKEN_KEY = 'asfdhkgjlr;ofijhgbfdklfsadf';
	private static $currentUserId;
	// private $pdo;

	// public function __construct()
	// {
	// 	//Load database configuration
	// 	$config = Configuration::getInstance();
	// 	$databaseConfig = $config->getConfig()['database'];
	// 	// print_r($databaseConfig);

	// 	//Connect to database
	// 	$this->pdo = new PDO(sprintf('mysql:host=%s;dbname=%s;charset=UTF8',$databaseConfig['host'],$databaseConfig['dbname']), $databaseConfig['username'], $databaseConfig['password'], [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"]);
	// }
	public function getByEmail($email)
	{
		$parameters=[
			':email' => $email
		];
		return $this->fetch('SELECT * FROM user where email = :email',$parameters);
			
	}

	public function getByUserId($userId)
	{
		$parameters=[
			':user_id' => $userId
		];
		return $this->fetch('SELECT * FROM user where user_id = :user_id',$parameters);
			
	}

	public function getList()
	{
		
		return $this->fetchAll('SELECT * FROM user');
	}


// 	private function fetchAll($sql, $parameters = [], $type=PDO::FETCH_ASSOC)
// 	{
// 		//Prepare statement
// 		$statement = $this->getPdo()->prepare($sql);

// 		//Bind parameters
// 		foreach ($parameters as $key => $value) {
// 			$statement->bindParam($key, $value, is_int($value) ? PDO::PARAM_INT : PDO::PARAM_STR);
// 		}
// 		//Execute
// 		$statement->execute();

// 		//Fetch all
// 		return $statement->fetchAll($type);
// 	}

// private function fetch($sql, $parameters = [], $type=PDO::FETCH_ASSOC)
// 	{
// 		//Prepare statement
// 		$statement = $this->getPdo()->prepare($sql);

// 		//Bind parameters
// 		foreach ($parameters as $key => $value) {
// 			$statement->bindParam($key, $value, is_int($value) ? PDO::PARAM_INT : PDO::PARAM_STR);
// 		}
// 		//Execute
// 		$statement->execute();

// 		//Fetch all
// 		return $statement->fetch($type);
// 	}

	public function insert($name, $email,$password)
	{
		//Hash password
		$passwordhash = password_hash($password, PASSWORD_BCRYPT);

		//prepare parameters
		$parameters = [
			':name' => $name,
			':email' => $email,
			':password' => $passwordhash
		];

		$rows = $this->execute('INSERT INTO user (name, email, password) VALUES (:name, :email, :password)', $parameters);

		return $rows==1;
	}
	public function verify($email, $password)
	{
		//Step 1 - Retrieve user
		$user = $this->getByEmail($email);
		//print_r($user);

		//Step 2 - Verify user password
		return password_verify($password, $user['password']);
	}

	public function generateToken($userId, $csrf = '')	
	{
	// Create token payload
	$payload = [
	    'user_id' => $userId,
	    'csrf' => $csrf ?: md5(time())
	];
	$payloadEncoded = base64_encode(json_encode($payload));
	$signature = hash_hmac('sha256', $payloadEncoded, self::TOKEN_KEY);

	return sprintf('%s.%s', $payloadEncoded, $signature);
}
public static function getTokenPayload($token)
{
    // Get payload and signature
    [$payloadEncoded] = explode('.', $token);

    // Get payload
    return json_decode(base64_decode($payloadEncoded), true);
}

public function verifyToken($token)
{
    // Get payload
    $payload = $this->getTokenPayload($token);
    $userId = $payload['user_id'];
    $csrf = $payload['csrf'];

    // Generate signature and verify
    return $this->generateToken($userId, $csrf) == $token;
}

	public static function verifyCsrf($csrf)
	{
		return self::getCsrf() == $csrf;
	}

	public static function getCsrf()
	{
		//Get token payload
		$token = $_COOKIE['user_token'];
		$payload = self::getTokenPayload($token);

		return $payload['csrf'];
	}

	// protected function getPdo()
	// {
	// 	return $this->pdo;
	// }

	public static function getCurrentUserId()
	{
		return self::$currentUserId;
	}

	public static function setCurrentUserId($userId)
	{
		self::$currentUserId = $userId;
	}
}
