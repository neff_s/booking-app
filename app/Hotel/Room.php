<?php

namespace Hotel;
use PDO;
use DateTime;
use Hotel\BaseService;

class Room extends BaseService
{
	public function get($roomId)
	{
		$parameters=[
			':room_id' => $roomId
		];
		return $this->fetch('SELECT * FROM room where room_id = :room_id',$parameters);
	}

	public function getCities()
	{
		//Get distinct cities
		$cities = [];
		try{
			
			$rows = $this->fetchAll('SELECT DISTINCT city FROM room');
			foreach ($rows as $row) {
				$cities[]=$row['city'];	
			}
		}catch (Exception $ex) {

		}
		return $cities;
	}

 	
 	public function getAreas()
	{
		//Get all areas
		$areas = [];
		$rows = $this->fetchAll('SELECT area FROM room');
		foreach ($rows as $row) {
			$areas[]=$row['area'];	
		}
		return $areas;
	}

	

	public function getGuests()
	{
		//Get distinct count of guests
		$countOfGuests = [];
		$rows = $this->fetchAll('SELECT DISTINCT count_of_guests FROM room');
		foreach ($rows as $row) {
			$countOfGuests[]=$row['count_of_guests'];	
		}
		return $countOfGuests;
	}
	

	
	public function search($checkInDate, $checkOutDate, $city = '', $typeId = '', $minimumPrice = '', $maximumPrice = '', $countOfGuests = '')
	{
		//Set up parameters
		$parameters=[
			':check_in_date' => $checkInDate->format(DateTime::ATOM),
			':check_out_date' => $checkOutDate->format(DateTime::ATOM)
		];

		if (!empty($city)){
			$parameters[':city'] = $city;
		}
		if (!empty($typeId)){
			$parameters[':type_id'] = $typeId;
		}
		if (!empty($minimumPrice)){
			$parameters[':minimumPrice'] = $minimumPrice;
		}
		if (!empty($maximumPrice)){
			$parameters[':maximumPrice'] = $maximumPrice;
		}
		if (!empty($countOfGuests)){
			$parameters[':countOfGuests'] = $countOfGuests;
		}
		//Build query
		$sql = 'SELECT  room.*, room_type.title FROM room INNER JOIN room_type ON room.type_id = room_type.type_id WHERE ';
		if (!empty($city)){
			$sql .= 'city = :city AND ';
		}
		if (!empty($typeId)){
			$sql .= 'room.type_id = :type_id AND ';
		}
		if (!empty($minimumPrice)){
			$sql .= 'room.price >= :minimumPrice AND ';
		}
		if (!empty($maximumPrice)){
			$sql .= 'room.price <= :maximumPrice AND ';
		}
		if (!empty($countOfGuests)){
			$sql .= 'room.count_of_guests = :countOfGuests AND ';
		}
		$sql .= 'room_id NOT IN (SELECT room_id FROM booking WHERE check_in_date <= :check_out_date AND check_out_date >= :check_in_date)';

		
		//Get results
		return $this->fetchAll($sql, $parameters);

		//execute SQL
		// $statement = $this->getPdo()->prepare('SELECT  * FROM room WHERE city = :city AND type_id = :type_id AND room_id NOT IN (SELECT room_id from booking WHERE check_in_date <= :check_out_date AND check_out_date >= :check_in_date)');

		// //Bind parameters
		// $statement->bindParam(':city', $city, PDO::PARAM_STR);
		// $statement->bindParam(':type_id', $typeId, PDO::PARAM_INT);
		// $statement->bindParam(':check_in_date', $checkInDate->format(Datetime::ATOM), PDO::PARAM_STR);
		// $statement->bindParam(':check_out_date', $checkOutDate->format(Datetime::ATOM), PDO::PARAM_STR);

		// $statement->execute();

		// //Get Rooms
		// $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
		// return $rows;
	}
	// protected function getPdo()
	// {
	// 	return $this->pdo;
	// }
}