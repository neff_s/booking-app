(function ($) {
$(document).on('submit', 'form.favoriteForm', function(e) {
	//stop default form behaviour
	e.preventDefault();

	//Get form data
	const formData = $(this).serialize();
	
	//Ajaz request
	$.ajax(
		'http://localhost/public/ajax/room_favorite.php',
		{
			type: "POST",
			dataType: "json",
			data: formData
			}).done(function (result){
				
				if(result.status){
					$('input[name=is_favorite]').val(result.is_favorite ? 1 : 0);
					$('.heart').toggleClass('selected', result.is_favorite);
				}else {
					$('.heart').toggleClass('selected', !result.is_favorite);
					

				}
			});
	
});

$(document).on('submit', 'form.stars-form', function(e) {
	//stop default form behaviour
	e.preventDefault();

	//Get form data
	const formData = $(this).serialize();
	
	//Ajaz request
	$.ajax(
		'http://localhost/public/ajax/room_review.php',
		{
			type: "POST",
			dataType: "html",
			data: formData
			}).done(function (result){
				// Append results to container
				$('.room-reviews-container').append(result);

				//reset review form
				$('form.stars-form').trigger('reset');
			});
	
});


})(jQuery);