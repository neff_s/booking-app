(function ($) {
$(document).on('submit', 'form.searchForm', function(e) {
	//stop default form behaviour
	e.preventDefault();

	//Get form data
	const formData = $(this).serialize();
	console.log(formData);
	//Ajaz request
	$.ajax(
		'http://localhost/public/ajax/search_results.php',
		{
			type: "GET",
			dataType: "html",
			data: formData
			}).done(function (result){
				//clear result container
				$('#search-results-container').html('');
				// Append results to container
				$('#search-results-container').append(result);

				//Push url state
				history.pushState({},'', 'http://localhost/public/list_page.php?' + formData);
			});
	
});

})(jQuery);